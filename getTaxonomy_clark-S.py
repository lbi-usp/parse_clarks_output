"""
Get_Taxonomy_Clark-S: get full lineage from taxonomy classification output 
comming from the Clark-S software.

Copyright (C) 2016, Antonio Diaz Tula

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys, os, csv, time
from os.path import join, isfile

# Ranks that will be included in the full lineage
allowedRank = ["superkingdom", "phylum", "class", "order", "family", "genus", "species"]

def getFormattedLineage(originalId, ncbi):
    # I just found that some taxon ids have been merged and ete3 does not handle them well, e.g 269483 was merged with 482957, but get_lineage(269483) returns [1]
    # Workaround is to translate taxid to name and then back to taxid
    nameList = ncbi.get_taxid_translator([originalId])
    if len(nameList) == 0:
        return "root|NA|0"
    name = nameList[originalId]
    idL = ncbi.get_name_translator([name])[name]
    if originalId in idL:
        id = originalId
    else:
        id = idL[-1]
    #if id != originalId:
    #    print originalId, id, name
    lineage = ncbi.get_lineage(id)
    if len(lineage) == 1 and ncbi.get_taxid_translator(lineage)[lineage[0]] == "root": # Only root
        formattedLineage = "root|NA|0"
    else:
        lineageList = []
        for lin in lineage:
            if lin != 0:
                name = ncbi.get_taxid_translator([lin])[lin]
                rank = ncbi.get_rank([lin])[lin]
                if rank in allowedRank:
                    lineageList.append("%s|%s|%i"%(rank, name, lin))
        if len(lineageList) > 0:
            formattedLineage = "\t".join(lineageList)
        else:
            formattedLineage = "root|NA|0"
    return formattedLineage

def getTaxonomy(srcDir, outDir, readNameColumn, taxIdColumn, skipHeader = True, srcFileSuffix = "", sep = "\t", secondOption = -1):
    # Validating directories
    if not os.path.isdir(srcDir):
        print "Source dir %s not found"%srcDir
        sys.exit(1)
    if not os.path.isdir(outDir):
        try:
            os.makedirs(outDir)
        except e:
            print "Could not create output file %s, error message: "%outDir, e
    # --------------------------------------------
    ncbi = NCBITaxa()
    files = [f for f in os.listdir(srcDir) if isfile(join(srcDir, f)) and f.endswith(srcFileSuffix)]
    print "%i files found in the source dir"%len(files)
    files.sort()
    if True:
        fileIdxs = []
        taxIds   = []
        for f in files:
            print "Reading entries from file : ", f
            fileIn = csv.reader(open(join(srcDir, f), "rt"), delimiter=sep)
            if skipHeader:
                fileIn.next()
            skipThisLine = False
            for i, l in enumerate(fileIn):
                taxId = 0
                try:
                    taxId = int(l[taxIdColumn])
                except:
                    if secondOption != -1:
                        try:
                            taxId = int(l[secondOption])
                            # print "peguei do 2o"
                        except:
                            continue
                    else:
                        continue
                if taxId != 0:
                    fileIdxs.append(i)
                    taxIds.append(taxId)
        print "Total number of entries        : ", len(taxIds)
        singletonList = list(set(taxIds))
        print "Number of unique IDs           : ", len(singletonList)
        lineageDict = dict()
        for i, id in enumerate(singletonList):
            # getting complete lineage
            formattedLineage = getFormattedLineage(id, ncbi)
            lineageDict[id] = formattedLineage
        print "Number of full lineages created: ", len(lineageDict.keys())
        print "=========================================================="
    # =============================================
    # Populate files entries
    # Create table with lineage for each read
    for f in files:
        print "Writting full lineage for %s in %s"%(f, f+"_full_lineage.tsv")
        fileIn = csv.reader(open(join(srcDir, f), "rt"), delimiter=sep)
        if skipHeader:
            fileIn.next()
        fout   = open(join(outDir, f+"_full_lineage.tsv"), "wt")
        skipThisLine = False
        conut = 0
        for l in fileIn:
            try:
                taxId = int(l[taxIdColumn])
            except:
                if secondOption != -1:
                    try:
                        taxId = int(l[secondOption])
                    except:
                        continue
                else:
                    continue
            if taxId != 0:
                lin = lineageDict[taxId]
            else:
                lin = "NA"
            fout.write("%s\t%s\n"%(l[readNameColumn].split(" ")[0].replace("@", "").replace(">", "").strip(), lin))

if __name__ == "__main__":
    try:
        from ete3 import NCBITaxa
    except:
        print "Module ete3 not installed, cannot get full lineage"
        sys.exit(1)
    if len(sys.argv) < 3:
        print "Use: python %s <source_dir> <output_dir> [files_suffix]"%sys.argv[0]
        print "Where: "
        print "<source_dir> is the directory with the Clark-S output files."
        print "<outout_dir> is the directory to write the results."
        print "[files_suffix] is the suffix of files to process in the source dir (e.g. the extension or some pattern that source files end with. Default is to process all files in the source dir."
        sys.exit(1)
    srcDir = sys.argv[1]
    outDir = sys.argv[2]
    if len(sys.argv) == 4:
        suffix = sys.argv[3]
    else:
        suffix = ""
    getTaxonomy(srcDir, outDir, readNameColumn = 0, taxIdColumn = 3, skipHeader = False, srcFileSuffix = suffix, sep=",", secondOption = 5)

