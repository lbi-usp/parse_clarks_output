# README #

This small script parses Clark-S output files to generate the full lineage for each sequence classification in the output file.

### Instructions for use ###

Simply download the file **getTaxonomy_clark-S.py** and run it like this: 

```
#!python
python getTaxonomy_clark-S.py <source_dir> <output_dir> [files_suffix]
```

Where:

* <source_dir> is the directory with the Clark-S output files.
* <output_dir> is the directory to write the results.
* [files_suffix] is the suffix of files to be processed in the source dir. Default is to process all files in the source dir.

### Example ###

The dir **clarkS-output/** contains a file with a typical Clark-S output. 

```
#!python
Object_ID,Length,Gamma,1st_assignment,score1,2nd_assignment,score2,confidence
AIBO1129.x1,1090,1.71981,208226,1823,NA,0,1
```

This read was taken from the **simHC.20.500** dataset extracted from here: [http://clark.cs.ucr.edu/Download/simHC.20.500.fa.gz](http://clark.cs.ucr.edu/Download/simHC.20.500.fa.gz)

To produce the full lineage of the Clark-S classification, run:
```
#!python
python getTaxonomy_clark-S.py clarkS-output/ clarkS-full_lineage/ _classif_clark.csv
```

This will produce the file **simHC.20.500_classif_clark.csv_full_lineage.tsv** in the output dir *clarkS-full_lineage/*. 

```
#!python
AIBO1129.x1     superkingdom|Bacteria|2 phylum|Firmicutes|1239  class|Clostridia|186801 order|Clostridiales|186802      Family|Clostridiaceae|31979     genus|Alkaliphilus|114627       species|Alkaliphilus metalliredigens|208226
```

The output file contains a line for each sequence in the input file with the full lineage. The full lineage is composed by ranks defined in the source file, e.g.


```
#!python
allowedRank = ["superkingdom", "phylum", "class", "order", "family", "genus", "species"]
```

You can change this list with the ranks you are interested in. Ranks in the output file are separated by a '\t' character. Each rank contains the rank level, the rank name, and the taxon ID separated by a '|' character. 

### Dependencies ###

Python 2.7 with the [ete3](https://pypi.python.org/pypi/ete3/) package installed.